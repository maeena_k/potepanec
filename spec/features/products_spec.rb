require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  let!(:ruby) { create(:taxon, name: 'ruby') }
  let!(:rails) { create(:taxon, name: 'rails') }
  let!(:tshirt) { create(:taxon, name: 'tshirt') }
  let!(:bag) { create(:taxon, name: 'bag') }
  let!(:ruby_tshirt) { create(:product, name: 'ruby_tshirt', taxons: [ruby, tshirt]) }
  let!(:ruby_bag) { create(:product, name: 'ruby_bag', taxons: [ruby, bag]) }
  let!(:rails_tshirt) { create(:product, name: 'rails_tshirt', taxons: [rails, tshirt]) }
  let!(:rails_bag) { create(:product, name: 'rails_bag', taxons: [rails, bag]) }

  before do
    visit potepan_product_path(ruby_tshirt.id)
  end

  scenario 'user sees a product page with related products' do
    expect(page).to have_title "#{ruby_tshirt.name} - BIGBAG Store"
    expect(page).to have_selector 'h2', text: ruby_tshirt.name
    expect(page).to have_selector 'h3', text: ruby_tshirt.display_price
    expect(page).to have_selector 'p', text: ruby_tshirt.description
    expect(page).to have_selector 'h5', text: ruby_bag.name
    expect(page).to have_selector 'h3', text: ruby_bag.display_price
    expect(page).to have_selector 'h5', text: rails_tshirt.name
    expect(page).to have_selector 'h3', text: rails_tshirt.display_price
  end

  scenario 'user goes to another related prduct from current product page' do
    click_on ruby_bag.name
    expect(page).to have_current_path potepan_product_path(ruby_bag.id)
  end
end
