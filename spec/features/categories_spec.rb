require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  let!(:category) { create(:taxonomy, name: 'category') }
  let!(:brand) { create(:taxonomy, name: 'brand') }
  let!(:bag) { create(:taxon, name: 'bag', taxonomy: category, parent: category.root) }
  let!(:clothing) { create(:taxon, name: 'clothing', taxonomy: category, parent: category.root) }
  let!(:shirt) { create(:taxon, name: 'shirt', taxonomy: category, parent: clothing) }
  let!(:product1) { create(:product, taxons: [bag]) }
  let!(:product2) { create(:product, taxons: [bag]) }
  let!(:product3) { create(:product, taxons: [shirt]) }

  before do
    visit potepan_category_path(bag.id)
  end

  scenario 'user sees a category page' do
    expect(page).to have_title "#{bag.name} - BIGBAG Store"
    expect(page).to have_selector 'h2', text: bag.name
    expect(page).to have_selector 'li.active', text: bag.name

    expect(page).to have_selector 'a', text: category.name

    click_on category.name
    expect(page).to have_selector 'a', text: "#{bag.name}（#{bag.products.count}）"
    expect(page).to have_selector 'a', text: clothing.name

    click_on clothing.name
    expect(page).to have_selector 'a', text: shirt.name

    expect(page).to have_selector 'h5', text: product1.name
    expect(page).to have_selector 'h3', text: product1.display_price
    expect(page).to have_selector 'h5', text: product2.name
    expect(page).to have_selector 'h3', text: product2.display_price
    expect(page).to have_no_content product3.name
  end

  scenario 'user goes to other category page from current category page' do
    click_on clothing.name
    click_on shirt.name
    expect(page).to have_current_path potepan_category_path(shirt.id)
  end

  scenario 'user goes to product page from category page' do
    click_on product1.name
    expect(page).to have_current_path potepan_product_path(product1.id)
  end
end
