require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:category) { create(:taxonomy, name: 'category') }
  let!(:brand) { create(:taxonomy, name: 'brand') }
  let(:bag) { create(:taxon, name: 'bag', taxonomy: category, parent: category.root) }
  let(:clothing) { create(:taxon, name: 'clothing', taxonomy: category, parent: category.root) }
  let(:shirt) { create(:taxon, name: 'shirt', taxonomy: category, parent: clothing) }
  let(:product1) { create(:product, taxons: [bag]) }
  let(:product2) { create(:product, taxons: [bag]) }
  let(:product3) { create(:product, taxons: [shirt]) }

  describe 'show action' do
    before do
      get :show, params: { id: bag.id }
    end

    it 'returns a 200 status code' do
      expect(response).to have_http_status 200
    end

    it 'renders show template' do
      expect(response).to render_template :show
    end

    it 'assigns @taxon' do
      expect(assigns(:taxon)).to eq bag
    end

    it 'assigns @products' do
      expect(assigns(:products)).to match_array([product1, product2])
    end

    it 'assigns @taxonomies' do
      expect(assigns(:taxonomies)).to match_array([category, brand])
    end
  end
end
