require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let!(:ruby) { create(:taxon, name: 'ruby') }
  let!(:rails) { create(:taxon, name: 'rails') }
  let!(:tshirt) { create(:taxon, name: 'tshirt') }
  let!(:bag) { create(:taxon, name: 'bag') }
  let!(:ruby_tshirt) { create(:product, name: 'ruby_tshirt', taxons: [ruby, tshirt]) }
  let!(:ruby_bag) { create(:product, name: 'ruby_bag', taxons: [ruby, bag]) }
  let!(:rails_tshirt) { create(:product, name: 'rails_tshirt', taxons: [rails, tshirt]) }
  let!(:rails_bag) { create(:product, name: 'rails_bag', taxons: [rails, bag]) }

  describe 'show action' do
    before do
      get :show, params: { id: ruby_tshirt.id }
    end

    it 'returns a 200 status code' do
      expect(response).to have_http_status 200
    end

    it 'renders show template' do
      expect(response).to render_template :show
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq ruby_tshirt
    end

    it 'assigns @related_products' do
      expect(assigns(:related_products)).to match_array([ruby_bag, rails_tshirt])
    end

    it 'returns only 4 related products' do
      create_list(:product, 5, taxons: [ruby, tshirt])
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
